<?php
if (isset($_POST['register-form'])) {
  $pseudo = htmlspecialchars($_POST['register-user']);
  $mail = htmlspecialchars($_POST['register-mail']);
  $mail2 = htmlspecialchars($_POST['register-mail2']);
  $mdp = sha1($_POST['register-pass']);
  $mdp2 = sha1($_POST['register-pass2']);
  if (!empty($_POST['register-user']) && !empty($_POST['register-mail']) && !empty($_POST['register-mail2']) && !empty($_POST['register-pass']) && !empty($_POST['register-pass2'])) {
      $pseudolength = strlen($pseudo);
      if ($pseudolength <= 255 && $pseudolength >= 3) {
        $reqpseudo = $bdd->prepare('SELECT * FROM user WHERE pseudo = ?');
        $reqpseudo->execute(array($pseudo));
        $pseudoexist = $reqpseudo->rowCount();
        if ($pseudoexist == 0) {
          if ($mail == $mail2) {
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
              $reqmail = $bdd->prepare('SELECT * FROM user WHERE mail = ?');
              $reqmail->execute(array($mail));
              $mailexist = $reqmail->rowCount();
              if ($mailexist == 0) {
                if ($mdp == $mdp2) {
                  $mdplength = strlen($mdp);
                  if ($mdplength >= 8) {
                    $insertmbr = $bdd->prepare('INSERT INTO user(pseudo, mail, pwd) VALUES(?, ?, ?)');
                    $insertmbr->execute(array($pseudo, $mail, $mdp));
                    $erreur = 'Votre compte a bien été créé ! <a href="connexion.php">Me connecter</a>';
                  }else {
                    $erreur = 'Votre mot de passe doit faire un minimum de 8 caracteres.';
                  }
                } else {
                  $erreur = 'Vos mots de passes ne correspondent pas !';
                }
              } else {
                $erreur = 'Adresse mail déjà utilisée !';
              }
            } else {
              $erreur = "Votre adresse mail n'est pas valide !";
            }
          } else {
            $erreur = 'Vos adresses mail ne correspondent pas !';
          }
        } else {
          $erreur = 'Pseudo deja utilisé !';
        }
      } else {
        $erreur = 'Votre pseudo ne dispose pas d\'une taille réglementaire';
      }
  }else {
    $erreur = 'Tous les champs doivent être complétés !';
  }
}
?>
