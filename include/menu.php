<nav class="navbar navbar-default navbar-fixed-top ">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Sound's Good</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="accueil.php">Accueil<span class="sr-only">(current)</span></a></li>
        <li><a href="musique.php">Coup de cœur</a></li>
        <li><a href="festival.php">Festivals</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <?php if (!isset($_SESSION['id'])){?>
        <li><a href="#" data-toggle="modal" data-target="#login-modal">Connexion</a></li>
        <li><a href="#" data-toggle="modal" data-target="#inscription-modal">inscription</a></li>
          <?php }else{ ?>
              <li><a href="#"><?php echo $_SESSION['pseudo'];?></a></li>
              <li><a href="include/deconnexion.php">Déconnexion</a></li>
          <?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php if (!isset($_SESSION['id'])) { ?>
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="loginmodal-container">
      <h1>Connexion</h1><br>
      <form action="" method="post">
        <input type="text" name="login-mail" placeholder="Mail">
        <input type="password" name="login-pass" placeholder="Mot de passe">
        <input type="submit" name="login-form" class="login loginmodal-submit" value="Login">
      </form>

      <div class="login-help">
        <a href="#">Register</a> - <a href="#">Forgot Password</a>
      </div>
    </div>
  </div>
</div>
  <div class="modal fade" id="inscription-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
       aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="loginmodal-container">
        <h1>Inscription</h1><br>
        <form action="" method="post">
          <input type="text" name="register-user" placeholder="Pseudo">
          <input type="email" name="register-mail" placeholder="Votre mail">
          <input type="email" name="register-mail2" placeholder="Verif. mail">
          <input type="password" name="register-pass" placeholder="Mot de passe">
          <input type="password" name="register-pass2" placeholder="Verif. mot de passe">
          <input type="submit" name="register-form" class="login loginmodal-submit" value="Inscription">
        </form>
      </div>
    </div>
  </div>
  <?php
}
require 'variables.php';
include 'include/register.php';
include 'include/login.php';
?>