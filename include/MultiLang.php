<?php

$userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); //Récupère les 2 premiers caractères de la langue du navigateur
$userLang = isset($_GET['lang']) ? $_GET['lang'] : $userLang; //Tente de récupérer un ?lang="..." dans l'adresse du site pour remplacer la langue par défaut du navigateur
$userLang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : $userLang;

switch ($userLang) {
  case 'fr': //Francais
  include 'lang/fr.php';
  break;

  case 'en':  //Anglais
  include 'lang/en.php';
  break;

  default:
  include 'lang/fr.php'; // si aucune langue n'est déclarée on inclut en.php par défaut
  break;
}
