<?php

require 'variables.php';
if (isset($_POST['formajout'])) {
    if (!empty($_POST['nomfest']) && !empty($_POST['selectl'])) {
        if (empty($_POST['imgfest'])) {
            $imgf = 'nothing';
        } else {
            $imgf = $_POST['imgfest'];
        }

        $datef = $_POST['an'].'/'.$_POST['mois'].'/'.$_POST['jour'];

        $nomf = $_POST['nomfest'];
        $lieuf = $_POST['selectl'];
        $descf = '<h3>Le festival '.$nomf.' aura lieu le '.$datef.' dans le departement '.$lieuf.'<br />affiche du festival: '.$imgf.'</h3>';
        $insertfest = $bdd->prepare('INSERT INTO subfest(datefest, nomfest, imgfest, lieufest, descf) VALUES(?, ?, ?, ?, ?)');
        $insertfest->execute(array($datef, $nomf, $imgf, $lieuf, $descf));
    }
}
echo "Votre demande d'ajout de festival a bien été pris en compte.";
header('refresh:4;url=../accueil.php');
