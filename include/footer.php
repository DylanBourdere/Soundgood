<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-5 col-sm-offset-1 text-center eltfooter">
        <p>Créé par Dylan Bourdere, Loic Chapon et Guillaume Veyrat</p>
      </div>
      <div class="col-sm-3 text-center">
        <i class="fa fa-facebook" id="logofacebook"></i>
        <i class="fa fa-twitter" id="logotwitter"></i>
        <i class="fa fa-github" id="logogithub"></i>
      </div>
      <div class="col-sm-2 text-center eltfooter">
        <p>Mentions légales</p>
      </div>
    </div>
</footer>