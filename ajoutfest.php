<?php
session_start();
require 'Class/Autoloader.php';
Autoloader::register();
$head = new ConstructHead();
?>
<!DOCTYPE html>
<html>
<head>
  <?php include 'include/head.php'; ?>
</head>
<body>
  <?php include 'include/menu.php'?>
  <div class="conteneur">

    <div id="right">
      <form action="include/verifenvoi.php" method="post">
        <label for="datefest">Date du début du festival:</label><br />
        <select name="jour">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>
        <select name="mois">
          <option value="1">Janvier</option>
          <option value="2">Fevrier</option>
          <option value="3">Mars</option>
          <option value="4">Avril</option>
          <option value="5">Mai</option>
          <option value="6">Juin</option>
          <option value="7">Juillet</option>
          <option value="8">Aout</option>
          <option value="9">Septembre</option>
          <option value="10">Octobre</option>
          <option value="11">Novembre</option>
          <option value="12">Decembre</option>
        </select>
        <select name="an">
          <option value="2016">2016</option>
          <option value="2017">2017</option>
        </select>
        <br><br>
        <label for="nomfest">Nom du festival:</label><br>
        <input type="text" name="nomfest" placeholder="Intitulé du festival"><br /><br>
        <label for="imgfest">Image de l'affiche du festival:</label><br>
        <input type="text" name="imgfest" placeholder="Lien vers l'image du festival"><br /><br>
        <label for="selectl">Lieu du festival:</label><br>
        <select name="selectl">
          <option value="ardeche">Ardeche</option>
          <option value="loire">Loire</option>
          <option value="rhone">Rhone</option>
          <option value="isere">Isere</option>
          <option value="ain">Ain</option>
          <option value="drome">Drome</option>
        </select><br><br>
        <input type="hidden" name="MAX_FILE_SIZE" value="1000000"> <input type="file" name="photoS">
        <input type="submit" name="formajout" value="Envoyer">
      </form>
    </div>
  </body>
  </html>
