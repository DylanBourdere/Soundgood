<?php
session_start();

require 'include/variables.php';

if (isset($_GET['id']) && $_GET['id'] > 0) {
    $getid = intval($_GET['id']);
    $requser = $bdd->prepare('SELECT * FROM user WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch(); ?>
  <?php
  if (isset($_SESSION['id']) && $userinfo['id'] == $_SESSION['id']) {
      ?>
    <?php
    $userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); //Récupère les 2 premiers caractères de la langue du navigateur
    $userLang = isset($_GET['lang']) ? $_GET['lang'] : $userLang; //Tente de récupérer un ?lang="..." dans l'adresse du site pour remplacer la langue par défaut du navigateur
    if ($userLang == 'fr') {
        include 'lang/fr.php';
    } elseif ($userLang == 'de') {
        include 'lang/de.php';
    } // si la langue est 'fr' inclut fr.php
    elseif ($userLang == 'en') {
        include 'lang/en.php';
    } elseif ($userLang == 'ta') {
        include 'lang/ta.php';
    } elseif ($userLang == 'ru') {
        include 'lang/ru.php';
    } elseif ($userLang == 'es') {
        include 'lang/es.php';
    } elseif ($userLang == 'ci') {
        include 'lang/ci.php';
    }// si la langue est 'en' inclut en.php
    else {
        include 'lang/fr.php';
    } // si aucune langue n'est déclarée on inclut en.php par défaut
    require 'Class/Autoloader.php';
      Autoloader::register();
      $head = new ConstructHead(); ?>
    <!DOCTYPE html>
    <html>
    <head>
      <?php include 'include/head.php'; ?>
    </head>
    <body>
      <div class="container-fluid">
        <?php include 'include/menu.php'?>
        <div class="row">
          <div class="col-sm-2 col-xs-0 left">  </div>
          <div class="col-sm-9 col-xs-12 right">
            <h2>Profil de <?php echo $userinfo['pseudo']; ?></h2>
            <br /><br />
            Pseudo = <?php echo $userinfo['pseudo']; ?>
            <br />
            Mail = <?php echo $userinfo['mail']; ?>
            <br />

            <br />
            <a style="color:red" href="ajoutfest.php">Soumettre un festival</a><br>
            <a style="color:red" href="editionprofil.php">Editer mon profil</a><br>
            <a style="color:red" href="include/deconnexion.php">Se déconnecter</a><br>
            <?php

  } else {
      header('Location:accueil.php');
  } ?>
        </div>
      </div>
    </div>
    <?php
    include 'include/footer.php';
    if (isset($_SESSION['pseudo'])) {
        include 'chat/chat.php';
        include 'chat/chatjs.php';
    } ?>
  </body>
  </html>
  <?php

}
?>
