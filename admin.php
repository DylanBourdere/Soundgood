<?php session_start();
if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1) {
    $userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); //Récupère les 2 premiers caractères de la langue du navigateur
  $userLang = isset($_GET['lang']) ? $_GET['lang'] : $userLang; //Tente de récupérer un ?lang="..." dans l'adresse du site pour remplacer la langue par défaut du navigateur
  if ($userLang == 'fr') {
      include 'lang/fr.php';
  } elseif ($userLang == 'de') {
      include 'lang/de.php';
  } // si la langue est 'fr' inclut fr.php
  elseif ($userLang == 'en') {
      include 'lang/en.php';
  } elseif ($userLang == 'ta') {
      include 'lang/ta.php';
  } elseif ($userLang == 'ru') {
      include 'lang/ru.php';
  } elseif ($userLang == 'es') {
      include 'lang/es.php';
  } elseif ($userLang == 'ci') {
      include 'lang/ci.php';
  }// si la langue est 'en' inclut en.php
  else {
      include 'lang/fr.php';
  }
    require 'Class/Autoloader.php';
    Autoloader::register();
    $head = new ConstructHead(); ?>
  <!DOCTYPE html>
  <html>
  <head>
    <?php include 'include/head.php'; ?>
  </head>
  <body>
    <div class="container-fluid">
      <?php include 'include/menu.php';?>
      <div class="row">
        <div class="col-sm-2 col-xs-0 left">  <?php include 'include/affiche.php';?></div>
        <div class="col-sm-9 col-xs-12 right"><h2>Administration</h2></div>
      </div>
    </div>
    <?php include 'include/footer.php'; ?>
  </body>
  </html>
  <?php

}
?>
