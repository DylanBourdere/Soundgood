<?php
/*
*Classe facilitant la création du head d'une page html
*/
class ConstructHead
{

  /*
  *Défini le charset en utf-8
  */
  public function charset()
  {

    return '<meta charset="utf-8">';

  }

  /*
  *$var string
  *Donne un titre a la page html
  */
  public function title($title)
  {

    return '<title>'.$title.'</title>';

  }

  /*
  *$var string
  *Envoi le lien du css
  */
  public function css($link)
  {

    return '<link rel="stylesheet"href="'.$link.'">';

  }

  /*
  *Permet l'ajout de Twitter bootstrap
  */
  public function addBootstrap()
  {

    return '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">';
    return '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';

  }

  /*
  *Permet l'ajout de Jquery
  */
  public function addJquery()
  {

    return '<script
    			  src="https://code.jquery.com/jquery-3.1.0.js"
    			  integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
    			  crossorigin="anonymous"></script>';

  }

  /*
  *$var string
  *Envoi le lien de notre favicon
  */
  public function favicon($fav)
  {

    return '<link rel="shortcut icon" type="image/x-icon" href="'.$fav.'"/>';

  }

}
 ?>
