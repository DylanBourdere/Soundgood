<?php
class Form
{
    public $surround = "p";
    public $surroundselect = "select name='nom'";
    public $surroundendselect = "select";

    private function surround($html)
    {
      return "<{$this->surround}>$html</{$this->surround}>";
    }

    private function surroundselect($html)
    {
      return "<{$this->surroundselect}>$html</{$this->surroundendselect}>";
    }

    public function input($place, $name)
    {
        return $this->surround('<input type="text" placeholder="'.$place.'"name="'.$name.'" />');
    }

    public function inputpass($place, $name)
    {
      return $this->surround('<input type="password" placeholder="'.$place.'"name="'.$name.'" />');
    }

    public function inputmail($place, $name)
    {
      return $this->surround('<input type="email" placeholder="'.$place.'"name="'.$name.'" />');
    }

    public function inputsubmit($place, $name)
    {
        return $this->surround('<input type="Submit" value="'.$place.'"name="'.$name.'" />');
    }
    public function selectday($compteur)
    {
      for($i=1;$i<$compteur+1;$i++){
      echo '<option value='.$i.'>'.$i.'</option>';
    }
    }
    public function selectmonth(){
      for ($i=1;$i<13;$i++){
        switch ($i) {
        case 1:
          $month = 'Janvier';
          break 1;

        case 2:
          $month = 'Février';
        break 1;

        case 3:
          $month = 'Mars';
        break 1;

        case 4:
          $month = 'Avril';
          break 1;
        case 5:
          $month = 'Mai';
          break 1;

        case 6:
          $month = 'Juin';
          break 1;

        case 7:
          $month = 'Juillet';
          break 1;

        case 8:
          $month = 'Aout';
          break 1;

        case 9:
          $month = 'Septembre';
          break 1;

        case 10:
          $month = 'Octobre';
          break 1;

        case 11:
          $month = 'Novembre';
          break 1;

        case 12:
          $month = 'Décembre';
          break 1;

          default:
          $month = "null";
          break;
        }
        echo '<option value='.$i.'>'.$month.'</option>';
      }
    }
    public function selectyears($start,$end){
      for ($start=$start; $start < $end+1 ; $start++) {
        echo '<option value='.$start.'>'.$start.'</option>';
      }
    }
    public function checkbox($name){
      return '<input type="checkbox" value="'.$name.'">';
    }
}
?>
