<?php

session_start();
require_once 'model.php';
$bdd = bdd();

if (isset($_SESSION['pseudo']) && isset($_GET['message'])) {
    $messag = banword($_GET['message']);
    ajout_message($bdd, $_SESSION['pseudo'], $messag);
    expire_message($bdd);
}
