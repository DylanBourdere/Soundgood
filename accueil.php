<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
  <?php include 'include/head.php';?>
</head>
<body>
<?php include 'include/menu.php';?>

<div class="container" id="containermenu">
  <div class="jumbotron">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 text-center">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna
          aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. D
          uis aute irure dolor in reprehenderit in voluptate velit es
          se cillum dolore eu fugiat nulla pariatur. E
          xcepteur sint occaecat cupidatat non proident, sunt in culpa
          qui officia deserunt mollit anim id est laborum.
        </p>
        <img src="#" alt="" class="img-responsive">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna
          aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. D
          uis aute irure dolor in reprehenderit in voluptate velit es
          se cillum dolore eu fugiat nulla pariatur. E
          xcepteur sint occaecat cupidatat non proident, sunt in culpa
          qui officia deserunt mollit anim id est laborum.
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna
          aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. D
          uis aute irure dolor in reprehenderit in voluptate velit es
          se cillum dolore eu fugiat nulla pariatur. E
          xcepteur sint occaecat cupidatat non proident, sunt in culpa
          qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
    </div>
  </div>
</div>
<?php include 'include/footer.php'; ?>
</body>
  </html>
