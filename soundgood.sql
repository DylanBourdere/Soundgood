-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 01 Juillet 2016 à 13:53
-- Version du serveur :  5.6.30-0ubuntu0.15.10.1
-- Version de PHP :  5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `soundgood`
--

-- --------------------------------------------------------

--
-- Structure de la table `Festivals`
--

CREATE TABLE IF NOT EXISTS `Festivals` (
  `id` int(11) NOT NULL,
  `lieux` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `festival` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Festivals`
--

INSERT INTO `Festivals` (`id`, `lieux`, `date`, `festival`) VALUES
(3, 'ardeche', '2016-06-16', '<div id="all-fest">\n  <h1>Festival Aluna</h1><br>\n  <img id="aluna" src="img/lune-300x300.png"/>\n  <a id="link-fest" href="#">en savoir plus</a>\n</div>\n'),
(4, 'ain', '2016-07-12', '<div id="all-fest">\n  <h1>Festival de Nimes</h1><br>\n  <img id="aluna" src="img/festnimes.jpg"/>\n  <a id="link-fest" href="#">en savoir plus</a>\n</div>\n');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `mail`, `pwd`) VALUES
(5, 'admin', '', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
(6, 'loperianesq', 'loperanes@gmail.com', '204a5aa16b5ea2671df9e7c763810974562cbc83'),
(7, 'loperianes', 'adressebidon@gmail.com', '204a5aa16b5ea2671df9e7c763810974562cbc83'),
(8, 'loperianess', 'jean-dupond@aol.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(9, 'loperianeloperianeloperianeloperianeloperiane', 'liko@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Festivals`
--
ALTER TABLE `Festivals`
ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Festivals`
--
ALTER TABLE `Festivals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
