<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <?php include 'include/head.php'; ?>
</head>
<body>
<?php include 'include/menu.php' ?>


    <?php if (isset($_GET['fest'])){
        echo /** @lang html */
        '
<div class="container" id="containermenu">
        <div class="jumbotron">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 text-center">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat. D
                        uis aute irure dolor in reprehenderit in voluptate velit es
                        se cillum dolore eu fugiat nulla pariatur. E
                        xcepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.
                    </p>
                    <img src="#" alt="" class="img-responsive">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat. D
                        uis aute irure dolor in reprehenderit in voluptate velit es
                        se cillum dolore eu fugiat nulla pariatur. E
                        xcepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.
                    </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat. D
                        uis aute irure dolor in reprehenderit in voluptate velit es
                        se cillum dolore eu fugiat nulla pariatur. E
                        xcepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
    </div>';
}else{?>
        <div class="container-fluid" id="containermenu">
        <div class="row">
    <div class="col-sm-10 col-sm-offset-1  display-form">Cliquez ici pour plus d'options<span id="plus"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></div>
    <div class="col-sm-10 col-sm-offset-1 form-choice"><form method="post" action="">
            div
            <p>choisir le mois:</p>
        <select>
            <option value="novembre">Novembre</option>
            <option value="decembre">Decembre</option>
            <option value="janvier">Janvier</option>
            <option value="fevrier">Février</option>
        </select>
            <p>choisir le lieux:</p>
        <select>
            <option value="ardeche">Ardeche</option>
            <option value="ain">Ain</option>
            <option value="drome">Drome</option>
            <option value="loire">Loire</option>
        </select>
            <p>Validez:</p>
        <input type="submit" value="Rechercher">
      </form>
    </div>
  </div>
</div>
<div class="container text-center">
  <div class="row">
    <div class="col-sm-6 col-md-3" id="test"><div id="all-fest">
        <h2>Festival Aluna</h2><br>
        <h2>Du <br><span id="date">22/07</span><br> au <br><span id="date">26/07</span></h2>
        <a id="link-fest" href="festival.php?fest=aluna">en savoir plus</a>
      </div>
    </div>
    <div class="col-sm-6 col-md-3" id="test"></div>
    <div class="col-sm-6 col-md-3" id="test"></div>
    <div class="col-sm-6 col-md-3" id="test"></div>
  </div>
</div>
<div class="container text-center">
  <div class="row">
    <div class="col-sm-6 col-md-3" id="test"></div>
    <div class="col-sm-6 col-md-3" id="test"></div>
    <div class="col-sm-6 col-md-3" id="test"></div>
    <div class="col-sm-6 col-md-3" id="test"></div>
  </div>
</div>
<?php }include 'include/footer.php'; ?>
<script>
    $(document).ready(function(){
        $(".fa-arrow-right").click(function(){
                $(".form-choice").slideDown("slow");
                $(".display-form").css("border-bottom", "none");
                $(".form-choice").css("border", "1px solid gray");
                $(".form-choice").css("border-top", "none");
                $(".fa-arrow-right").css("transform", "rotate(90deg)");
        });
    });
</script>
</body>
</html>