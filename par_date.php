<?php session_start(); ?>
<?php
$userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); //Récupère les 2 premiers caractères de la langue du navigateur
$userLang = isset($_GET['lang']) ? $_GET['lang'] : $userLang; //Tente de récupérer un ?lang="..." dans l'adresse du site pour remplacer la langue par défaut du navigateur
if ($userLang == 'fr') {
    include 'lang/fr.php';
} elseif ($userLang == 'de') {
    include 'lang/de.php';
} // si la langue est 'fr' inclut fr.php
elseif ($userLang == 'en') {
    include 'lang/en.php';
} elseif ($userLang == 'ta') {
    include 'lang/ta.php';
} elseif ($userLang == 'ru') {
    include 'lang/ru.php';
} elseif ($userLang == 'es') {
    include 'lang/es.php';
} elseif ($userLang == 'ci') {
    include 'lang/ci.php';
}// si la langue est 'en' inclut en.php
else {
    include 'lang/en.php';
} // si aucune langue n'est déclarée on inclut en.php par défaut
require 'Class/Autoloader.php';
Autoloader::register();
$head = new ConstructHead();
?>
<!DOCTYPE html>
<html>
<head>
  <?php include 'include/head.php'; ?>
</head>
<body>
  <div class="container-fluid">
    <?php include 'include/menu.php'?>
    <div class="row">
      <div class="col-sm-2 col-xs-2 left">  </div>
      <div class="col-sm-9 right">
        <div class="text-center">
          <a href="?juillet">Juillet</a> |
          <a href="?aout">Aout</a> |
          <a href="?septembre">Septembre</a> |
          <a href="?octobre">Octobre</a> |
          <a href="?novembre">Novembre</a> |
          <a href="?decembre">Decembre</a> |
        </div>
        <?php
        if (strpos($_SERVER['REQUEST_URI'], 'juillet') !== false) {
            $i = 'juillet';
            include 'include/choixdate.php';
        }
        if (strpos($_SERVER['REQUEST_URI'], 'aout') !== false) {
            $i = 'aout';
            include 'include/choixdate.php';
        }
        if (strpos($_SERVER['REQUEST_URI'], 'septembre') !== false) {
            $i = 'septembre';
            include 'include/choixdate.php';
        }
        if (strpos($_SERVER['REQUEST_URI'], 'octobre') !== false) {
            $i = 'octobre';
            include 'include/choixdate.php';
        }
        if (strpos($_SERVER['REQUEST_URI'], 'novembre') !== false) {
            $i = 'novembre';
            include 'include/choixdate.php';
        }
        if (strpos($_SERVER['REQUEST_URI'], 'decembre') !== false) {
            $i = 'decembre';
            include 'include/choixdate.php';
        }
        ?>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
    </div>
  </div>
  <?php
  include 'include/footer.php';
  if (isset($_SESSION['pseudo'])) {
      include 'chat/chat.php';
      include 'chat/chatjs.php';
  }
  ?>
</body>
</html>
