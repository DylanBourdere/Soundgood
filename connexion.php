<?php session_start(); ?>
<!DOCTYPE html>
<?php
include 'include/login.php';
?>
<html>
<head>
  <?php include 'include/head.php'; ?>
</head>
<body>
  <div class="container-fluid">
    <?php include 'include/menu.php'; ?>
    <div class="row">
      <div class="col-sm-2 col-xs-2 left">  <?php include 'include/affiche.php'; ?></div>
      <div class="col-sm-9 col-xs-12 right">
        <div align="center">
          <h2><?php echo $langconnexion; ?></h2>
          <br /><br />
          <form method="POST" action="">
            <?php
            echo $form->inputmail('Mail', 'mailconnect');
            echo $form->inputpass('Mot de passe', 'mdpconnect');
            echo $form->inputsubmit('Envoyer', 'formconnexion');
            ?>
          </form>
          <?php
          if (isset($erreur)) {
              echo '<font color="red">'.$erreur.'</font>';
          }
          ?>
        </div>
      </div>
      <script type="text/javascript">
      $(document).ready(function() {
        $('#log').hide(0).delay(250).fadeIn(1000);
        $('#pass').hide(0).delay(500).fadeIn(1000);
        $('#sub').hide(0).delay(750).fadeIn(1000);
      })
      </script>
    </div>
  </div>
</body>
</html>
